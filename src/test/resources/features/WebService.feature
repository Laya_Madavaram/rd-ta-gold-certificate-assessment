Feature: Automate the webservice of books.

  Scenario Outline: Create a user login by using the API and Validate the user login from web UI.
    Given the URI of the books webservice API
    When a POST request made using username "<username>" and password "<password>"
    Then validate status code and response body
    And navigate to the login page of books webservice
    And login to the page with credentials and click login button
    Then validate username on the books dashboard page

    Examples:
    | username | password   |
    | john_k32 | p1L@s_w^d  |


  Scenario: Get all book details from the API call and validate the details from UI.
    Given the URI of the books webservice API
    When response code is validated
    And navigate to the books page
    Then validate the obtained response using UI