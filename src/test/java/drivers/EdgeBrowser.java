package drivers;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.time.Duration;

public class EdgeBrowser implements Browser {
    private WebDriver driver;

    @Override
    public WebDriver getDriver() {
        WebDriverManager.edgedriver().setup();
        this.driver = new EdgeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        return driver;
    }

    @Override
    public void closeDriver() {
        this.driver.close();
        this.driver.quit();
    }
}