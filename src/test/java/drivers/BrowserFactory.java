package drivers;
import lombok.SneakyThrows;
import org.openqa.selenium.WebDriver;
import java.io.FileInputStream;
import java.util.Properties;

public class BrowserFactory {
    public static Browser browser = null;
    private static final Properties properties = new Properties();

    @SneakyThrows
    private static void loadProperties() {
        String path = "src/test/resources/browser.properties";
        FileInputStream fis = new FileInputStream(path);
        properties.load(fis);
    }

    public static WebDriver getBrowserDriver() {
        loadProperties();
        String browserType = properties.getProperty("browser");
        if(browserType.equals("chrome")) {
            browser = new ChromeBrowser();
        }
        else if(browserType.equals("edge")) {
            browser = new EdgeBrowser();
        }
        else {
            browser = new FirefoxBrowser();
        }
        return browser.getDriver();
    }

    public static void close() {
        browser.closeDriver();
    }
}