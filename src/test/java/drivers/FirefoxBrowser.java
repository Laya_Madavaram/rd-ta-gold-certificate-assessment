package drivers;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;

public class FirefoxBrowser implements Browser {
    private WebDriver driver;

    @Override
    public WebDriver getDriver() {
        WebDriverManager.firefoxdriver().setup();
        this.driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        return driver;
    }

    @Override
    public void closeDriver() {
        this.driver.close();
        this.driver.quit();
    }
}