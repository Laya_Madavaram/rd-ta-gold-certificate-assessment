package stepdefs;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class WebServiceStepdefs {
    private static Response userCreationResponse;
    private static Response booksDataResponse;
    private static String username;
    private static String password;
    WebDriver driver;
    PagesContext pagesContext;

    public WebServiceStepdefs(PagesContext pagesContext) {
        this.pagesContext = pagesContext;
        this.driver = pagesContext.getDriver();
    }

    @Given("the URI of the books webservice API")
    public void setURIOfBooksWebservice() {
        RestAssured.baseURI = "https://demoqa.com/";
    }

    @When("a POST request made using username {string} and password {string}")
    public void createUser(String name, String passkey) throws JSONException {
        RestAssured.basePath = "Account/v1/User";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName",name);
        jsonObject.put("password",passkey);
        userCreationResponse = given()
                .body(jsonObject.toString())
                .header("Content-type","application/json")
                .when()
                .post();
        username = name;
        password = passkey;
    }

    @Then("validate status code and response body")
    public void validateUserCreationResponse() {
        Assert.assertEquals(userCreationResponse.statusCode(),201);
        userCreationResponse.then().assertThat().body("username",equalTo(username));
    }

    @And("navigate to the login page of books webservice")
    public void navigateToTheLoginPageOfBooksWebservice() {
        driver.get("https://demoqa.com/login");
    }

    @And("login to the page with credentials and click login button")
    public void login() {
        pagesContext.getLoginPage().login(username,password);
    }

    @Then("validate username on the books dashboard page")
    public void validateUsernameOnTheBooksDashboardPage() {
        String name = pagesContext.getProfilePage().getUsernameText();
        Assert.assertEquals(name,username);
    }

    @When("response code is validated")
    public void responseCodeIsValidated() {
        RestAssured.basePath = "BookStore/v1/Books";
        booksDataResponse = given()
                .header("Content-type","application/json")
                .when()
                .get();
        Assert.assertEquals(booksDataResponse.statusCode(),200);
        booksDataResponse.prettyPrint();
    }

    @And("navigate to the books page")
    public void navigateToTheBooksPage() {
        driver.navigate().to("https://demoqa.com/books");
    }

    @Then("validate the obtained response using UI")
    public void validateTheObtainedResponseUsingUI() {
        ArrayList<Map<String,String>> books = booksDataResponse.path("books");
        books.forEach(book -> {
            pagesContext.getBooksPage().getBookDetails(book.get("title"));
            String author = pagesContext.getBookDetailsPage().getAuthor();
            String publisher = pagesContext.getBookDetailsPage().getPublisher();
            Assert.assertEquals(author,book.get("author"));
            Assert.assertEquals(publisher,book.get("publisher"));
        });
    }
}