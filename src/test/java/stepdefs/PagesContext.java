package stepdefs;
import com.automate.books.DriverManager;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import pages.*;

public class PagesContext {
    private static WebDriver driver;
    private LoginPage loginPage;
    private ProfilePage profilePage;
    private BooksPage booksPage;
    private BookDetailsPage bookDetailsPage;

    public WebDriver getDriver() {
        return driver;
    }

    @Before(order = 1)
    public void setDriver() {
        driver = DriverManager.launchBrowser();
    }

    @Before(order = 2)
    public void initializePageObjects() {
        loginPage = LoginPage.getLoginPage(driver);
        profilePage = ProfilePage.getProfilePage(driver);
        booksPage = BooksPage.getBooksPage(driver);
        bookDetailsPage = BookDetailsPage.getBookDetailsPage(driver);
    }

    public LoginPage getLoginPage() {
        return loginPage;
    }

    public ProfilePage getProfilePage() {
        return profilePage;
    }

    public BooksPage getBooksPage() {
        return booksPage;
    }

    public BookDetailsPage getBookDetailsPage() {
        return bookDetailsPage;
    }
}