package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BookDetailsPage {
    private static BookDetailsPage bookDetailsPage;
    WebDriver driver;
    @FindBy(xpath = "//*[@id='author-wrapper']//*[@id='userName-value']")
    WebElement author;
    @FindBy(xpath = "//*[@id='publisher-wrapper']//*[@id='userName-value']")
    WebElement publisher;

    private BookDetailsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public static BookDetailsPage getBookDetailsPage(WebDriver driver) {
        BookDetailsPage obj = bookDetailsPage;
        if(obj == null) {
            bookDetailsPage = obj = new BookDetailsPage(driver);
        }
        return obj;
    }

    public String getAuthor() {
        return author.getText();
    }

    public String getPublisher() {
        return publisher.getText();
    }
}