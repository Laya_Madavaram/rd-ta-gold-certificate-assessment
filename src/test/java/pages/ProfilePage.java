package pages;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage {
    private static ProfilePage profilePage;
    WebDriver driver;
    @FindBy(css = "#userName-value")
    private WebElement usernameText;

    private ProfilePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public static ProfilePage getProfilePage(WebDriver driver) {
        ProfilePage obj = profilePage;
        if(obj == null) {
            profilePage = obj = new ProfilePage(driver);
        }
        return obj;
    }

    public String getUsernameText() {
        try {
            String name = usernameText.getText();
            return name;
        }
        catch(NoSuchElementException exception) {
            System.out.println(exception.getMessage());
        }
        return "";
    }
}