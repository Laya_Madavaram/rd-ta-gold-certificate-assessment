package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private static LoginPage loginPage;
    WebDriver driver;
    @FindBy(id = "userName")
    private WebElement usernameInput;
    @FindBy(id = "password")
    private WebElement passwordInput;
    @FindBy(id = "login")
    private WebElement loginButton;

    private LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public static LoginPage getLoginPage(WebDriver driver) {
        LoginPage obj = loginPage;
        if(obj == null) {
            loginPage = obj = new LoginPage(driver);
        }
        return obj;
    }

    public void login(String username,String password) {
        try {
            usernameInput.sendKeys(username);
            passwordInput.sendKeys(password);
            loginButton.submit();
        }
        catch(Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
}