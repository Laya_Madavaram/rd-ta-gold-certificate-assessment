package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BooksPage {
    private static BooksPage booksPage;
    WebDriver driver;
    private WebElement title;

    private BooksPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public static BooksPage getBooksPage(WebDriver driver) {
        BooksPage obj = booksPage;
        if(obj == null) {
            booksPage = obj = new BooksPage(driver);
        }
        return obj;
    }

    public void getBookDetails(String bookTitle) {
        try {
            title = driver.findElement(By.linkText(bookTitle));
            title.click();
        }
        catch(NoSuchElementException exception) {
            System.out.println(exception.getMessage());
        }
    }
}