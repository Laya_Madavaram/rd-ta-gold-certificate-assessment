package com.automate.books;
import drivers.BrowserFactory;
import org.openqa.selenium.WebDriver;

public class DriverManager {
    public static WebDriver launchBrowser() {
        return BrowserFactory.getBrowserDriver();
    }

    public static void quitBrowser() {
        BrowserFactory.close();
    }
}