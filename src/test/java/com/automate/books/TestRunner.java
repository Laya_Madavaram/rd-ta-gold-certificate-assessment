package com.automate.books;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@CucumberOptions(glue = {"stepdefs"},features = {"src/test/resources/features"},
        plugin = {"pretty","html:report.html","json:report.json"})
public class TestRunner extends AbstractTestNGCucumberTests {
}